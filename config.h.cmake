/* config.h.cmake  Substituted by CMake  */

/* Define this if you have alloca.h */
#cmakedefine HAVE_ALLOCA_H 1

/* Define this if you have glob.h */
#cmakedefine HAVE_GLOB_H 1

/* Define this if you have sys/wait.h */
#cmakedefine HAVE_SYS_WAIT_H 1

/* Define this if you have signal.h */
#cmakedefine HAVE_SIGNAL_H 1

/* Define this if you have sys/stat.h */
#cmakedefine HAVE_SYS_STAT_H 1

/* Define this if you have sys/types.h */
#cmakedefine HAVE_SYS_TYPES_H 1

/* Define this if you have unistd.h */
#cmakedefine HAVE_UNISTD_H 1

/* Define this if you have the lstat() function */
#cmakedefine HAVE_LSTAT 1

/* Define this if you have the fcntl() function */
#cmakedefine HAVE_FCNTL 1

/* Define this if you have the readlink() function */
#cmakedefine HAVE_READLINK 1

/* Define this if you have the sethostname() function */
#cmakedefine HAVE_SETHOSTNAME 1

/* Define this if you have the glob() function */
#cmakedefine HAVE_GLOB 1

/* Define this if your compiler supports alloca() */
#cmakedefine HAVE_ALLOCA 1

/* Define this if your libc has sys_siglist[] */
#cmakedefine HAVE_SYS_SIGLIST 1

/* Define to the full name of this package. */
#cmakedefine PACKAGE_NAME "@PACKAGE_NAME@"

/* Define to the one symbol short name of this package. */
#cmakedefine PACKAGE_TARNAME "@PACKAGE_TARNAME@"

/* Define to the version of this package. */
#cmakedefine PACKAGE_VERSION "@PACKAGE_VERSION@"

/* Define this if you have the 'sigset_t' type */
#cmakedefine HAVE_SIGSET_T 1

/* Define this if you have fcntl.h */
#cmakedefine HAVE_FCNTL_H 1


