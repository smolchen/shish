#ifndef SIG_H
#define SIG_H

#include <signal.h>
#include <unistd.h>

void sig_block(void);

#endif /* SIG_H */
